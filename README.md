# Uncertainty-aware sequence assessment

## Introduction

This repository contains code for the uncertainty-aware assessment of image sequences. It utilizes the ChickenNet backbone model (an extended MASK-RCNN model) together with an uncertainty metric and clustering approach to assess sequences.
Therefore, despite the main sequence_assessment.py script, it also contains the backbone model ("/mrcnn") as well as a deep sort tracker ("/deep_sort) which can be added to assign individual detections to each other. By default, the tracking is disabled and ground truth id information are used.


## Running the sequence assessment
Execute the desired command based on the dataset and uncertainty type:

**Please Note:** Depending on the dataset path, the ground truth information paths may need to be changed. Refer to [lines 926-946](./sequence_assessment.py#L926-L946) in the `sequence_assessment.py` file for the relevant code.


   - Run sequence assessment on the MARS-Attributes dataset using aleatoric uncertainty:
     ```
     python3 sequence_assessment.py --type person --uncertainty aleatoric --input_folder_path <MARS-Attributes Test Set Path>
     ```
     
   - Run sequence assessment on the MARS-Attributes dataset using epistemic uncertainty:
     ```
     python3 sequence_assessment.py --type person --uncertainty epistemic --input_folder_path <MARS-Attributes Test Set Path>
     ```
     
   - Run sequence assessment on the chicken dataset using aleatoric uncertainty:
     ```
     python3 sequence_assessment.py --type chicken --uncertainty aleatoric --input_folder_path <Chicken Dataset Test Set Path>
     ```
     
   - Run sequence assessment on the chicken dataset using epistemic uncertainty:
     ```
     python3 sequence_assessment.py --type chicken --uncertainty epistemic --input_folder_path <Chicken Dataset Test Set Path>
     ```

   Replace `<MARS-Attributes Test Set Path>` with the actual path to the MARS-Attributes test set folder, and `<Chicken Dataset Test Set Path>` with the actual path to the chicken dataset test set folder.

The script will execute and perform the sequence assessment using the specified parameters.


