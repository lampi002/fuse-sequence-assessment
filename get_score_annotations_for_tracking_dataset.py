import json
import glob
import os 
import numpy as np

tracking_images_dir = '/media/christian/SamsungSSD/ZED/datasets/tracking_dataset/hen_tracking_val/images'
tracking_file = open('/media/christian/SamsungSSD/ZED/datasets/tracking_dataset/hen_tracking_val/releases/coco_tracking_val/output.json')
tracking_annotations = json.load(tracking_file)
tracking_file.close()
scoring_images_dir = '/media/christian/SamsungSSD/ZED/datasets/1500_images_with_damages_val_occlusion/val'
scoring_file = open('/media/christian/SamsungSSD/ZED/datasets/1500_images_with_damages_val_occlusion/output.json')
scoring_annotations = json.load(scoring_file)
scoring_file.close()
 
def get_dataset(annotation_file, images_dir, id_or_score):
     # Get all annotations from the dataset. 
    annotations = {}
    for annotation in annotation_file['annotations']:
        image_id = annotation['image_id']
        if image_id not in annotations:
            annotations[image_id] = []
        annotations[image_id].append(annotation)

    # Get image path for images from the dataset 
    seen_images = {}
    instance_annotations = {}
    for image in annotation_file['images']:
        image_id = image['id']
        if image_id in seen_images:
            print("Warning: Skipping duplicate image id: {}".format(image))
        else:
            seen_images[image_id] = image
            try:
                image_file_name = image['file_name']
            except KeyError as key:
                print("Warning: Skipping image (id: {}) with missing key: {}".format(image_id, key))

            find_file = glob.glob(images_dir + "/**/" + image_file_name, recursive = True) # search in all subdire$
            if len(find_file) >0:
               image_path = os.path.abspath(os.path.join(find_file[0]))
            else:
               image_path = "not_found"
               continue
            try:
               image_annotations = annotations[image_id]
               for index, annotation in enumerate(image_annotations):
                  sequence_name = os.path.dirname(image_path).split('/')[-1]
                  file_name = os.path.basename(image_path)
                  file_name = file_name[:10]
                  if sequence_name == "Futterlinie_Stufe_11_2020_09_23-16_09_57_HD2K":
                    print("sequence name", sequence_name)
                    print("image_path: ", file_name)
                  #print("image id", image_id)
                  annotation.update({'image_path':file_name})
                  if sequence_name not in instance_annotations:
                     if id_or_score == "id":
                        instance_annotations[sequence_name] = {}
                     else:
                        instance_annotations[sequence_name] = []
                  if id_or_score == "id":
                      instance_id = annotation['extra']['instance_id']
                      if sequence_name == "Futterlinie_Stufe_11_2020_09_23-16_09_57_HD2K":
                        print("instance_id: ", instance_id)
                      if instance_id not in instance_annotations[sequence_name]:
                         instance_annotations[sequence_name][instance_id] = []
                      instance_annotations[sequence_name][instance_id].append(annotation)
                  elif id_or_score == "score":
                     #score_attributes = [element for element in  annotation['extra']['attributes'] if len(element) < 3]
                     #annotation.update({'image_path':image_path})
                     instance_annotations[sequence_name].append(annotation)
            except KeyError:
               print("No annotation found for image ", image_id)
               continue;
     
    return instance_annotations

def compute_iou(box1, box2):
    """Calculates IoU of the given box with the array of the given boxes.
    box1: 1D vector [y1, x1, y2, x2]
    box2: 1D vector [y1, x1, y2, x2]
    """
    # Calculate areas: 
    box1_area = box1[2]* box1[3]
    box2_area = box2[2]* box2[3]

    # Calculate intersection areas
    x_min = np.maximum(box1[0], box2[0])
    y_min = np.maximum(box1[1], box2[1])
    x_max = np.minimum(box1[0] + box1[2], box2[0] + box2[2])
    y_max = np.minimum(box1[1] + box1[3], box2[1] + box2[3])
    intersection = np.maximum(x_max - x_min, 0) * np.maximum(y_max - y_min, 0)
    union = box1_area + box2_area - intersection
    iou = intersection / union
    return iou


def edit_json_file(original_annotations, new_annotations):
   images=original_annotations['images']
   old_annotations = original_annotations['annotations']

   for index, old_annotation in enumerate(old_annotations): 
     old_annotation_id = old_annotation['id'] 
     # find corresponding annotation in new annotations, bzw. pro Sequenz jede ID finden und davon den Score abgreifen 
     for sequence in new_annotations:
        for instance in new_annotations[sequence]:
           for element in new_annotations[sequence][instance]:
              new_annotation_id = element['id']
              if old_annotation_id == new_annotation_id:
                 score = element['score']
                 original_annotations['annotations'][index].update({'score':score})
   with open('ids+scores.json', 'w') as outfile:
      json.dump(original_annotations, outfile)
                          
          
tracking_dataset = get_dataset(tracking_annotations, tracking_images_dir, "id")
score_dataset = get_dataset(scoring_annotations, scoring_images_dir, "score")


#create a dataset that contains both the ids and scores  
for tracking_sequence in tracking_dataset:
 #if tracking_sequence == 'Futterlinie_Stufe_12_2020_09_02-14_24_27_HD2K':
   print(tracking_sequence)
   print("    -------------------------    ")
   for instance in tracking_dataset[tracking_sequence]:
      instance_scores = []
      print("Instance:", instance)
      instance_score = -1 
      for an in tracking_dataset[tracking_sequence][instance]:
         image_path = an['image_path']
         for score_sequence in score_dataset:
            # find matching sequence and matching image
            if score_sequence == tracking_sequence:
               for score_an in score_dataset[score_sequence]:
                  class_id = score_an['category_id']                 
                  if image_path == score_an['image_path']:
                     if class_id == 0:
                        score_attributes = [element for element in  score_an['extra']['attributes'] if len(element) < 3]
                        if len(score_attributes) > 0:
                           if int(score_attributes[0]) == -1:
                              try:
                                score = int([element for element in score_an['extra']['attributes'] if "gt" in element][0].split(":")[-1])
                              except:
                                continue
                           else:
                              score = int(score_attributes[0])
                           iou = compute_iou(score_an['bbox'], an['bbox'])
                           # find matching instance 
                           if iou > 0.85:
                              #print("IOU: ",iou)
                              instance_scores.append(score)
                              instance_score = np.mean(instance_scores)
      #add score for all images of this instance within this sequence
      for element in tracking_dataset[tracking_sequence][instance]:
         element.update({'score':instance_score})
      print("score: ", instance_score)            
      print("---------------------------------")

edit_json_file(tracking_annotations, tracking_dataset)



#for annotation in annotations: 
#  annotation_id = annotation['id'] 
#  print(annotation_id)
#  old_category_id = annotation['category_id']
#  attribute = annotation['extra']['attributes'][0]
#  if attribute not in new_categories:  # collect all new categories 
#   new_categories.append(attribute)
#  new_category_id = new_categories.index(attribute)
#  annotation['category_id'] = new_category_id + 1 # +1 because 0 is background 
#  if(annotation['image_id']==223):
#   print(annotation) 

#categories = []
#for category in new_categories:
#  category_element = {
#  "id": new_categories.index(category),
#  "name": category,
#  "supercategory": "root"
#  }
#  categories.append(category_element)
