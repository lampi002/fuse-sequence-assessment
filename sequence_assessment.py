import os
import sys
import json
import numpy as np
import matplotlib.pyplot as plt
import cv2
import argparse
import glob
import skimage
import math
import datetime

from PIL import Image, ImageDraw


from sklearn.cluster import KMeans, MeanShift, estimate_bandwidth
from sklearn.decomposition import PCA
 
from deep_sort import nn_matching
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker
from application_util import preprocessing 
from tools import generate_detections as gdet

# Root directory of the project
ROOT_DIR = '../'
assert os.path.exists(ROOT_DIR), 'ROOT_DIR does not exist. Did you forget to read the instructions above? ;)'

# Import mrcnn libraries
sys.path.append(ROOT_DIR) 
from mrcnn.config import Config
import mrcnn.utils as utils
from mrcnn import visualize
from mrcnn.utils import compute_matches, compute_ap, compute_f1 
import mrcnn.model_quality_estimator_calibrated as modellib
#import mrcnn.model_age_estimation as modellib


class ChickenDataset(utils.Dataset):

    """ Generates a COCO-like dataset, i.e. an image dataset annotated in the style of the COCO dataset.
        See http://cocodataset.org/#home for more information.
    """
    def load_data(self, annotation_json, images_dir):
        """ Load the coco-like dataset from json
        Args:
            annotation_json: The path to the coco annotations json file
            images_dir: The directory holding the images referred to by the json file
        """
        # Load json from file
        min_file_size = 10000000
        json_file = open(annotation_json)
        coco_json = json.load(json_file)
        json_file.close()
        
        # Add the class names using the base method from utils.Dataset
        source_name = "coco_like"
        for category in coco_json['categories']:
           class_id = category['id'] 
           class_name = category['name'] 
           if class_id < 1:
                print('Error: Class id for "{}" cannot be less than one. (0 is reserved for the background)'.format(class_name))
                return
           self.add_class(source_name, class_id, class_name)
         
        classes = []     # to have a list which just contains the class names and the number of instances for each 
        for element in self.class_info:
           class_entry = {
                         'id' : element['id'],
                         'name' : element['name'],
                         'count' : 0
                        }
           classes.append(class_entry)
 
        # Get all annotations
        annotations = {}
        for annotation in coco_json['annotations']:
            image_id = annotation['image_id']
            if image_id not in annotations:
                annotations[image_id] = []
            annotations[image_id].append(annotation)
        
        # Get all images and add them to the dataset
        seen_images = {}
        for image in coco_json['images']:
            image_id = image['id']
            if image_id in seen_images:
                print("Warning: Skipping duplicate image id: {}".format(image))
            else:
                seen_images[image_id] = image
                try:
                   #image_file_name = format(image['id'], '08d')+"_"+image['file_name']
                    image_file_name = image['file_name'] 
                    print(image_file_name)
                    image_width = image['width']
                    image_height = image['height']
                except KeyError as key:
                    print("Warning: Skipping image (id: {}) with missing key: {}".format(image_id, key))

                #find_file = glob.glob(images_dir + "/**/" + format(image['id'], '08d')+"*", recursive = True) # search in all subdirectories 
                find_file = glob.glob(images_dir + "/**/" + image_file_name, recursive = True) # search in all subdirectories
                print(find_file)
                if len(find_file) >0:
                   image_path = os.path.abspath(os.path.join(find_file[0]))               
                else:
                   image_path = "not_found"
                   #print("Image_path: ", image_path)
                try:
                   image_annotations = annotations[image_id]
                except KeyError:
                   print("No annotation found for image ", image_id)
                   continue;
                hasCrowd = False # Quick fix to avoid problems with "count" annotations. If more than 1segmentation/object -> skip image
                for element in image_annotations:
                  if element['iscrowd']:
                    hasCrowd = True 
                
                # Add the image using the base method from utils.Dataset (if file exists)
                if os.path.isfile(image_path):
                 if not hasCrowd:  # to avoid that images of object with "multiple parts" are added
                  for annotation in image_annotations:
                     id = (annotation['category_id'])
                     for class_element in classes:
                        if class_element['id'] == id:
                           # Increment the counter by 1
                           class_element['count'] += 1
                  self.add_image(
                      source=source_name,
                      image_id=image_id,
                      path=image_path,
                      width=image_width,
                      height=image_height,
                      annotations=image_annotations)

        print("...........Dataset-Info:.......................\n")
        for i in range(len(classes)):
           print ("Class: {}, Instances: {}".format(classes[i]['name'], classes[i]['count']))
 
    def load_mask(self, image_id):
        """ Load instance masks for the given image.
        MaskRCNN expects masks in the form of a bitmap [height, width, instances].
        Args:
            image_id: The id of the image to load masks for
        Returns:
            masks: A bool array of shape [height, width, instance count] with
                one mask per instance.
            class_ids: a 1D array of class IDs of the instance masks.
            scores and quality: estimators for the plumage conditon and the quality of the detection 
            instance_ids: a 1D array of instance ids for the individual detections 
     """
        image_info = self.image_info[image_id]
        annotations = image_info['annotations']
        instance_masks = []
        class_ids = []
        scores = []
        qualities = []
        instance_ids = []
        
        for annotation in annotations:
            class_id = annotation['category_id']
            score = annotation['score']
            instance_id = annotation['extra']['instance_id']
            quality = -1 
            mask = Image.new('1', (image_info['width'], image_info['height']))
            mask_draw = ImageDraw.ImageDraw(mask, '1')
            for segmentation in annotation['segmentation']:
                mask_draw.polygon(segmentation, fill=1)
                bool_array = np.array(mask) > 0
                instance_masks.append(bool_array)
                class_ids.append(class_id)
                scores.append(score)
                qualities.append(quality)
                instance_ids.append(instance_id)

        mask = np.dstack(instance_masks)
        class_ids = np.array(class_ids, dtype=np.int32)
        scores = np.array(scores, dtype=np.float32)
        qualities = np.array(qualities, dtype=np.float32)
        instance_ids = np.array(instance_ids, dtype=np.int32)

        return mask, class_ids, scores, qualities, instance_ids

class MarsDataset(utils.Dataset):

    """ Generates a COCO-like dataset, i.e. an image dataset annotated in the style of the COCO dataset.
        See http://cocodataset.org/#home for more information.
    """
    
    def load_data(self, annotation_csv, images_dir, images_per_track=20):
        """ Load the dataset from csv
        Args:
            annotation_csv: The path to the annotations csv file
            images_dir: The directory holding the images referred to by the csv file
            images_per_track: Number of images to be considered per tracklet
        """
                
        # Add the class names using the base method from utils.Dataset
        source_name = "coco_like"
        class_id = 1 
        class_name = 'Person'
        self.add_class(source_name, class_id, class_name)
        
        
        # Get all annotations
        # Dictionary contains all annotations by person_id 
        annotations = {}
                
        with open(annotation_csv, 'r') as file:
              for index, line in enumerate(file):
                 if index > 0:
                     annotation = line.rstrip().split(',') 
                     person_id = int(annotation[0])
                     age_label = int(annotation[14])
                     occlusion = int(annotation[5])
                     #print("person_id:", person_id, " age: ", age_label, " occlusion: ", occlusion)
                     #this results in one annotation per person, therefore useful only for person related attributes but not for image attributes!
                     person_dict = {'person_id': person_id, 'age_label': age_label}
                     # add the dictionary to the annotations dictionary
                     annotations[person_id] = person_dict
             
        
        # Get all images and add them to the dataset
        image_id = 0
        number_of_persons = len(os.listdir(images_dir))
        image_count = 0  # initialize image count
        print("number of persons", number_of_persons) 

        tracklet_image_count = {}
        for root, dirs, files in os.walk(images_dir):
            for file_name in sorted(files):
                if file_name.endswith(".jpg"):
                    image_path = os.path.join(root, file_name)
                    try:
                        person_id = int(file_name[0:4])
                        tracklet_id = int(file_name[7:11])
                        tracklet_unique_id = str(person_id) + "_" + str(tracklet_id)
                        image = Image.open(image_path)
                        image_height, image_width = (image.size[1], image.size[0])
                        image_annotations = annotations[person_id]
                        print("person id, tracklet_id: ", person_id, tracklet_unique_id)
                    except Exception as e:
                        print("Exception occurred:", e)
                        continue  # skip this file if person ID is not an int or 0 
                    
                    # check if the maximum number of images per tracklet has been reached
                    if tracklet_unique_id in tracklet_image_count and tracklet_image_count[tracklet_unique_id] >= images_per_track:
                        continue
             
                    dataset.add_image(
                        source=source_name,
                        image_id=image_id,
                        path=image_path,
                        width=image_width,
                        height=image_height,
                        annotations=image_annotations)
                    image_id += 1
                    
                    # increment the image count for the current tracklet
                    if tracklet_unique_id in tracklet_image_count:
                        tracklet_image_count[tracklet_unique_id] += 1
                    else:
                        tracklet_image_count[tracklet_unique_id] = 1
        print("Tracklets:", tracklet_image_count)
        print(len(tracklet_image_count), " tracklets found")
                        
                               
        
           
    def load_mask(self, image_id):
        """ Load instance masks for the given image.
        MaskRCNN expects masks in the form of a bitmap [height, width, instances].
        As our dataset does not contain mask or bounding box labels, we create them artifically and just use the age attribute.
        Args:
            image_id: The id of the image to load masks for
        Returns:
            masks: A bool array of shape [height, width, instance count] with
                one mask per instance.
            class_ids: a 1D array of class IDs of the instance masks.
            scores and quality: estimators for the plumage conditon and the quality of the detection 
            instance_ids: a 1D array of instance ids for the individual detections 
        """
        
        image_info = self.image_info[image_id]
        annotation = image_info['annotations']
        instance_masks = []
        class_ids = [1]
        scores = [annotation["age_label"]]
        qualities = [1]
        instance_ids = [annotation["person_id"]]
        
        mask = Image.new('1', (image_info['width'], image_info['height']))
        mask_draw = ImageDraw.ImageDraw(mask, '1')
        # fake segmentation:
        segmentation = (2,2,image_info['width']-2,image_info['height']-2)
        mask_draw.rectangle(segmentation, fill=1)
        bool_array = np.array(mask) > 0
        instance_mask = np.array([bool_array])
        
        mask = np.dstack(instance_mask)
        class_ids = np.array(class_ids, dtype=np.int32)
        scores = np.array(scores, dtype=np.float32)
        qualities = np.array(qualities, dtype=np.float32)
        instance_ids = np.array(instance_ids, dtype=np.int32)
        
        print("shapes: ", mask.shape, class_ids.shape, scores.shape)
        print("image_id: ", image_id, " score: ", scores)
        return mask, class_ids, scores, qualities, instance_ids


# this class creates an object for each individual (id) that aggregates all information for this id  
class Individual:
    def __init__(self, id):
       self.id = id
       self.scores = []
       self.uncertainties = []
       self.features = []
       self.feature_distances = [0]
       self.cropped_instances =[]

    def __repr__(self):
       return repr((self.id, self.scores, self.uncertainties))

    def add_element(self, score, uncertainty, feature, cropped_instance=None, uncertainty_type="aleatoric"):
       self.scores.append(score)
       if uncertainty_type == "aleatoric":
          self.uncertainties.append(math.exp(uncertainty)) # exp to obtain uncertainty value between 0 and 1
       elif uncertainty_type == "epistemic":
          self.uncertainties.append(uncertainty)
       self.features.append(feature)
       self.cropped_instances.append(cropped_instance)

    def get_best_score(self, uncertainty_thresh=0.25): #0.25 for aleatoric 
       """First get clusters. Per cluster choose most certain view if this view is below uncertainty threshold. then find maximum score among the best-of-cluster-scores 
       Args:
            uncertainty_thresh: Maximum uncertainty. Predictions with an uncertainty above this threshold will be rejected.
       """
       best_scores_per_cluster = []
       uncertainties_per_cluster = [] 
       if hasattr(self, 'clusters'):
          #for score, uncertainty, cluster in zip(self.scores, self.uncertainties, self.clusters):
          #   print("score: ", score, " uncertainty: ", uncertainty ," cluster: ", cluster ,"\n")
          for cluster_label in np.unique(self.clusters):
             # filter elements with an uncertainty below threshold
             #print("clusters: ", self.clusters)
             print("uncertainties:", self.uncertainties)
             #print("scores: ", self.scores)
             cluster_indices = np.where((self.clusters == cluster_label) & (np.array(self.uncertainties) <= uncertainty_thresh))[0]
             #print("cluster indices:", cluster_indices)
             cluster_scores = np.array(self.scores)[cluster_indices]
             cluster_uncertainties = np.array(self.uncertainties)[cluster_indices]
             if len(cluster_indices)>0:
                weighted_mean_of_cluster, uncertainty_of_mean, uncertainty_of_mean_neu = self.weighted_mean(cluster_scores, cluster_uncertainties)
                uncertainty_of_cluster = uncertainty_of_mean_neu #np.mean(cluster_uncertainties)
                #print("weighted average of cluster ", cluster_label, "is: ", weighted_mean_of_cluster)
                #print("uncertainties of cluster ", cluster_label, "is: ", cluster_uncertainties)
                #print("mean uncertainties of cluster ", cluster_label, "is: ", uncertainty_of_cluster)
                #print("COMPARISON: mean uncertainty: ", uncertainty_of_cluster, " vs uncertainty of mean: ", uncertainty_of_mean, " vs. standard error of mean: ", uncertainty_of_mean_neu)
                best_score_cluster = weighted_mean_of_cluster
             else:
                best_score_cluster = -1
                uncertainty_of_cluster = 1
                #print("no reliable assessment possible") 
             best_scores_per_cluster.append(best_score_cluster)
             uncertainties_per_cluster.append(uncertainty_of_cluster) 
          self.best_score = self.weighted_max(best_scores_per_cluster, uncertainties_per_cluster) 
          #self.best_score = self.weighted_mean(best_scores_per_cluster, uncertainties_per_cluster)[0]
          #self.best_score = best_scores_per_cluster[np.argmin(uncertainties_per_cluster)]
       else:
          self.best_score = -1
       return self.best_score
    
    def weighted_mean(self, scores, uncertainties):
      weights = np.reciprocal(np.square(uncertainties))
      normalized_weights = weights/np.sum(weights)
      # Multiply each value by its weight
      weighted_scores = [score * weight for score, weight in zip(scores, normalized_weights)]
      
      # Sum the weighted scores
      weighted_mean = sum(weighted_scores)
      uncertainty_of_mean = 1/np.sqrt(np.sum(weights))
      standard_error_of_mean = np.sqrt(1/np.sum(weights))/np.sqrt(len(weights))
      return weighted_mean, uncertainty_of_mean, standard_error_of_mean
   
    def weighted_max(self, scores, uncertainties):
       weights = np.reciprocal(np.square(uncertainties))
       normalized_weights = weights/np.sum(weights)
       # Multiply each value by its normalized weight
       weighted_scores = [score * weight for score, weight in zip(scores, normalized_weights)]
       # get the max of the weighted measurements 
       weighted_max_index = np.argmax(weighted_scores)
       max_score = scores[weighted_max_index]
 
       return max_score
    
    def max_of_ranges(self, scores, uncertainties):
       ranges = scores - uncertainties
       weighted_max_index = np.argmax(ranges)
       max_score = scores[weighted_max_index]
    
    def get_feature_distance_to_previous(self):
       # calculate distance of a feature to the previous feature
       distance = nn_matching._cosine_distance(self.features[-1], self.features[-2])[0]
       self.feature_distances.append(distance)
       return distance

    def k_means_clustering(self):
       from sklearn.metrics.pairwise import euclidean_distances
       from sklearn.metrics import silhouette_score
       features = np.array(self.features)
       print("shape: ",features.shape)
       nsamples, element, feature = features.shape
       features = features.reshape((nsamples,feature))
       sil = []
       kmax = min(nsamples,10)
       # dissimilarity would not be defined for a single cluster, thus, minimum number of clusters should be 2
       if nsamples > 2:
          for k in range(2, kmax):
             kmeans = KMeans(n_clusters = k).fit(features)
             labels = kmeans.labels_
             sil.append(silhouette_score(features, labels, metric = 'euclidean'))
          #print("sil: ", sil)
          high_sil_index = np.argmax(sil)
          optimal_k = high_sil_index + 2
          #print(" Optimal sil index: ", high_sil_index, " optimal k: ", optimal_k )
          kmeans = KMeans(n_clusters=optimal_k, random_state=0).fit(features)
          distances_to_centroid = euclidean_distances(np.array(kmeans.cluster_centers_), features)
          #if self.id==1 and nsamples == 130:
          #   print("----Storing clusters!----")
          #   for n, label in enumerate(kmeans.labels_):
          #      cv2.imwrite(str(self.id) + "-" + str(label) + " " + str(n) + ".png", self.cropped_instances[n])
          self.clusters = kmeans.labels_
          #print("Clusters: ", self.clusters)
          return kmeans.labels_
     
    
    def mean_shift_clustering(self):
       features = np.array(self.features)
       print("shape: ",features.shape)
       list_size = sys.getsizeof(features) / 1000000  # in MB
       print(f"The size of the list is {list_size} MB")
       nsamples, element, feature = features.shape
       features = features.reshape((nsamples,feature))
       # dissimilarity would not be defined for a single cluster, thus, minimum number of clusters should be 2
       if nsamples > 2:
          # Apply PCA to reduce dimensionality to 2 dimensions
          #features = np.squeeze(features)
          # perform PCA
          #pca = PCA(n_components=2)
          #features = pca.fit_transform(features)
                     
          bandwidth = estimate_bandwidth(features, n_jobs=-1)
          if bandwidth == 0:
                bandwidth = 1e-5
          print("bandwidth ", bandwidth)
          ms = MeanShift(bandwidth=bandwidth, n_jobs = -1).fit(features)
          labels = ms.labels_
          print(str(self.id), len(self.cropped_instances))
          #if nsamples == 100:
            # print("----Storing clusters!----")
            # # Plot the features and their clusters
            # fig, ax = plt.subplots()
            # scatter = ax.scatter(features[:, 0], features[:, 1], c=labels, cmap='viridis')
            # legend1 = ax.legend(*scatter.legend_elements(),
                                # loc="upper right", title="Clusters")
            # ax.add_artist(legend1)
            # plt.savefig("plot.png")
            #for n, label in enumerate(labels):
            #    cv2.imwrite("ID-" + str(self.id) + "-Cluster-" + str(label) + "-Instance-" + str(n) + ".png", cv2.cvtColor(self.cropped_instances[n], cv2.COLOR_RGB2BGR))
          
          self.clusters = labels    
          return labels

class ChickenConfig(Config):
    """
    Derives from the base Config class and overrides values specific
    to the cigarette butts dataset.
    """
    # Give the configuration a recognizable name
    NAME = "feather__damage"

    # Train on 1 GPU and 1 image per GPU. Batch size is 1 (GPUs * images/GPU).
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

    # Number of classes (including background)
    NUM_CLASSES = 1 + 2  # background + hen + damage

    # All of our training images are 512x512
    IMAGE_MIN_DIM = None
    IMAGE_MAX_DIM = 896 # must be dividable by 2 at least 6 times 

    # You can experiment with this number to see if it improves training
    STEPS_PER_EPOCH = 50 # 500

    # This is how often validation is run. If you are using too much hard drive space
    # on saved models (in the MODEL_DIR), try making this value larger.
    VALIDATION_STEPS = 10 # 5
    
    # Matterport originally used resnet101, but I downsized to fit it on my graphics card
    BACKBONE = 'resnet50'

    RPN_ANCHOR_SCALES = (8, 16, 32, 64, 128)
    TRAIN_ROIS_PER_IMAGE = 32
    MAX_GT_INSTANCES = 20 
    POST_NMS_ROIS_INFERENCE = 500 
    POST_NMS_ROIS_TRAINING = 1000
    USE_MINI_MASK = False 
    DETECTION_MIN_CONFIDENCE = 0.99
      

class MarsConfig(ChickenConfig):
      DETECTION_MIN_CONFIDENCE = 0.0
      NUM_CLASSES = 1 + 1  # background + person
      IMAGE_MAX_DIM = 128
      

def load_image(path, config):
    image = skimage.io.imread(path)
    # If has an alpha channel, remove it for consistency
    if image.shape[-1] == 4:
           image = image[..., :3]
    image, window, scale, padding, crop = utils.resize_image(
        image,
        min_dim=config.IMAGE_MIN_DIM,
        min_scale=config.IMAGE_MIN_SCALE,
        max_dim=config.IMAGE_MAX_DIM,
        mode=config.IMAGE_RESIZE_MODE)
    dimensions= image.shape
    print('dimensions:',dimensions)
    return image


def apply_mask(image, mask, color, alpha=0.2):
    """apply mask to image"""
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                  image[:, :, c] *
                                  (1 - alpha) + alpha * color[c] * 255,
                                  image[:, :, c])
    return image


def display_instances(image, boxes, masks, class_ids, class_names, scores=None, corrected_scores=None, uncertainties=None, ids=None,figsize=(16, 16), ax=None, show_mask=True, show_bbox=True):
    assert boxes.shape[0] == masks.shape[-1] == class_ids.shape[0]
    N = boxes.shape[0]
    if not N:
        print("\n*** No instances to display *** \n")
    else:
        assert boxes.shape[0] == masks.shape[-1] == class_ids.shape[0]

    masked_image = image.copy()
    print("scores in display: ", scores)
    for i in range(N):
        # plumage damages are marked in red
        if class_ids[i] == 2:
           color = [1,0,0]
           label = ""
        # uncertain scores are grey, score 0 is green, 1 is yellow and 2 is orange
        if class_ids[i] == 1:
           if corrected_scores[i] ==-1:
              color = [128/255,128/255,128/255]
              label = "Uncertain"
           elif corrected_scores[i] <= 0.5:
              color = [0,1,0]
              label = "Perfect plumage"
           elif corrected_scores[i] <= 1.5:
              color = [1,1,0]
              label = "Minor damage"
           if corrected_scores[i] > 1.5:
              color = [1, 136/255, 0]
              label = "Heavy damage"
           #label = str(scores[i])
           #print(label)
        #print('label=',label)
        if not np.any(boxes[i]):
            continue
        
        y1, x1, y2, x2 = boxes[i]
        class_id = class_ids[i]
        id = ids[i] if ids is not None else 0
        #label=str(id)  + " " + str(scores[i])
        score = scores[i] if scores is not None else None
        corrected_score = corrected_scores[i]
        uncertainty = uncertainties[i] if uncertainties is not None else None
        #if class_ids[i] != 2:  
        caption = "ID: {:.1f} \nScore: {:.1f} \nCorrected Score: {:.1f} \nUncertainty: {:.3f}\n".format(id,score,corrected_score,math.exp(float(uncertainty))) 
        #else:
        #   caption = None

        # Mask
        mask = masks[:, :, i]
        masked_image = apply_mask(masked_image, mask, color)
        color = [element * 255 for element in color]
        #create bounding box
        masked_image = cv2.rectangle(masked_image, (x1, y1), (x2, y2), tuple(color), 2)
        #create text box 
        w, h = 185, 60
        sub_img = masked_image[y1-h:y1, x1:x1+w]
        black_rect = np.ones(sub_img.shape, dtype=np.uint8) * 0
        res = cv2.addWeighted(sub_img, 0.3, black_rect, 0.7, 1.0)
        masked_image[y1-h:y1, x1:x1+w] = res
        #masked_image = cv2.rectangle(masked_image, (x1, y1-h), (x1 + w, y1), (0, 0, 0), -1)
        dy = 13
        for i, line in enumerate(caption.split('\n')):
           y = y1 + (i+1)*dy
           masked_image = cv2.putText(masked_image, line, (x1, y), cv2.FONT_HERSHEY_DUPLEX, 0.25, (255, 255, 255), 1, cv2.LINE_AA)
        
        #skimage.io.imsave(str(score) +".png", masked_image)
        
    return masked_image
     

def run_assessment(tracker, encoder, image, r, individual_dict, uncertainty="aleatoric", gt_ids=None, gt_bbox=None, gt_class_id=None, gt_mask=None):
        """ Update tracker and use tracking plus feature-based clustering to optimize the assessment.
            Create/Update objects of class Individual for each detected bird. Assigns detections, scores and uncertainties to each Individual.
        Args:
            tracker: The tracker object
            encoder: Encoder object to determine the features of each instance which are needed for clustering
            image: The image to be processed
            r: Result of the object detector which contains bboxes, masks, classes, scores and uncertainties
            individual_dict: dictionary of predictions and uncertainties for all individuals to be considered for the assessment. Either assessment on identity-level or tracklet-level (resets identity information after each tracklet)
            gt_ids (optional): Ground truth id to provide instead of ids from tracker  
        Returns:
            corrected_scores: Same shape as scores given by the input prediction. Values replaced by the new (updated) scores from our method.
            ids: ID for each detected object in prediction input.
        """
        
        features = encoder(image, r['rois'])
        detections = []
        colors = visualize.random_colors(r['rois'].shape[0])
        for bbox, class_score, classe, mask, color, feature in zip(r['rois'], r['class_scores'], r['class_ids'], r['masks'], colors, features):
            detections.append(Detection(bbox, class_score, classe, mask, color, feature))
        features = np.array([detection.feature for detection in detections])
        # if ground_truth provided, use gt_ids, otherwise use tracker
        if gt_ids is not None:
           class_id =1 
           gt_match, pred_match, overlaps = compute_matches(gt_bbox, gt_class_id, gt_mask, r["rois"], r["class_ids"], r["class_scores"], r['masks'], class_id)
           print("gt_match", gt_match)
           class_gt_instance_id = gt_ids[np.where(gt_class_id==class_id)]  # ground truth instance ids for the specific class in this image 
           pred_match_indices = pred_match[(pred_match>-1)].astype(int) # For each correctly predicted box it has the index of the matched gt box.    
           ids = class_gt_instance_id[pred_match_indices] # gt instance_ids of each correctly predicted box
           # filter detections to only get those that have ground_truth
           pred_with_gt_indices = np.where(pred_match>-1)[0]  # index of all predictions that have gt
           features_of_tracked = features[pred_with_gt_indices]  
           r = {k: np.take(v,pred_with_gt_indices, 0) if k!='masks' else np.take(v,pred_with_gt_indices, -1) for k, v in r.items()}
        else:
            tracker.predict()
            tracker.update(detections)
            # Get confirmed trackers (state = 2) that have a matched detection and sort trackers by det_idx to have same order as detections.
            matched_trackers = [track for track in tracker.tracks if track.det_idx!=None and track.state == 2]
            matched_trackers.sort(key=lambda x: x.det_idx, reverse=False)
            tracker_det_idx = [track.det_idx for track in matched_trackers]
            # filter detections to only get those that are tracked
            features_of_tracked = features[tracker_det_idx]
            r = {k: np.take(v,tracker_det_idx, 0) if k!='masks' else np.take(v, tracker_det_idx, -1) for k, v in r.items()}
            ids = np.array([int(track.track_id) for track in matched_trackers])
            
        cropped_instances = [] 
        for bbox in r['rois']:
           y1, x1, y2, x2 = bbox
           cropped_instance = np.array(image[y1:y2, x1:x2])
           cropped_instances.append(cropped_instance)
        #cropped_instances = np.array(cropped_instances)  
        feature_distances =[]
        for id in ids:
           if (id in individual_dict):
              print("Original prediction is: ", r['scores'][np.where(ids == id)][0])
              individual = individual_dict[id]
              if uncertainty == "aleatoric":
                #individual.add_element(r['scores'][np.where(ids == id)][0], r["score_variances"][np.where(ids == id)][0], features_of_tracked[np.where(ids == id)], uncertainty_type="aleatoric", cropped_instance=cropped_instances[np.where(ids == id)[0][0]])
                individual.add_element(r['scores'][np.where(ids == id)][0], r["score_variances"][np.where(ids == id)][0], features_of_tracked[np.where(ids == id)], uncertainty_type="aleatoric", cropped_instance=cropped_instances[np.where(ids == id)[0][0]])
                individual.mean_shift_clustering() 
                individual.get_best_score(uncertainty_thresh=0.25)
              elif uncertainty == "epistemic":
                  individual.add_element(r['scores'][np.where(ids == id)][0], r["score_certainties"][np.where(ids == id)][0], features_of_tracked[np.where(ids == id)], uncertainty_type="epistemic")
                  individual.mean_shift_clustering() 
                  individual.get_best_score(uncertainty_thresh=1.0)
              individual.get_feature_distance_to_previous()
              cv2.imwrite("ID-" + str(individual.id) + "-Instance-" + str(id) + "-Score-" + str(r['scores'][np.where(ids == id)][0]) + "-Corrected_Score" + str(individual.best_score) + ".png", cv2.cvtColor(cropped_instances[np.where(ids == id)[0][0]], cv2.COLOR_RGB2BGR))
              #print("Best score of id ", id, " is ", individual.best_score)
           else:
              new_individual = Individual(id)
              if uncertainty == "aleatoric":
                #new_individual.add_element(r['scores'][np.where(ids == id)][0], r["score_variances"][np.where(ids == id)][0], features_of_tracked[np.where(ids == id)], uncertainty_type="aleatoric", cropped_instance=cropped_instances[np.where(ids == id)[0][0]])    
                new_individual.add_element(r['scores'][np.where(ids == id)][0], r["score_variances"][np.where(ids == id)][0], features_of_tracked[np.where(ids == id)], uncertainty_type="aleatoric", cropped_instance=cropped_instances[np.where(ids == id)[0][0]])
                new_individual.get_best_score(uncertainty_thresh=0.25)
              elif uncertainty == "epistemic":
                new_individual.add_element(r['scores'][np.where(ids == id)][0], r["score_certainties"][np.where(ids == id)][0], features_of_tracked[np.where(ids == id)], uncertainty_type="epistemic")
                new_individual.get_best_score(uncertainty_thresh=1.0)
              individual_dict[id] = new_individual 
           
        corrected_scores = [individual_dict[id].best_score for id in ids]
        feature_distances = [individual_dict[id].feature_distances for id in ids]
        return r, corrected_scores, ids, individual_dict

def evaluate_image(filename, prediction, ids, corrected_scores, gt_class_id, gt_instance_id, gt_bbox, gt_mask, gt_score, gt_quality): 
       image_result = [[] for _ in range(len(dataset.class_info))] # results for each class 
       r = prediction
       
       #assuming we have only one class. Otherwise this needs to be changed
       class_id = 1
       gt_match, pred_match, overlaps = compute_matches(gt_bbox, gt_class_id, gt_mask, r["rois"], r["class_ids"], r["class_scores"], r['masks'], class_id)
       
       
       # predictions and uncertainties for each gt_instance, independent of true or not
       class_gt_instance_id = gt_instance_id[np.where(gt_class_id==class_id)]  # ground truth instance ids for the specific class in this image  
       class_gt_score = gt_score[np.where(gt_class_id==class_id)]  # ground truth scores for the specific class in this image
       class_pred_indices = np.where(r["class_ids"]==class_id) # indices of predictions that belong to class id 
       class_pred_instance_id = ids[class_pred_indices]
       class_pred_score_epistemic = r["score_certainties"][class_pred_indices]
       class_pred_score_aleatoric = r["score_variances"][class_pred_indices]
       
       # predictions that match a ground truth instance:
       gt_match_indices = gt_match[(gt_match>-1)].astype(int) # for each correctly matched gt box it has the index of the matched predicted box  
       print("gt_match_indices", gt_match_indices)
       if len(gt_match_indices)>0:
           pred_gt_instance_id = np.where(gt_match>-1 ,class_pred_instance_id[gt_match.astype(int)], np.nan)
           pred_score_epistemic = np.where(gt_match>-1 ,class_pred_score_epistemic[gt_match.astype(int)], np.nan)
           pred_score_aleatoric = np.where(gt_match>-1 ,class_pred_score_aleatoric[gt_match.astype(int)], np.nan)
      
       
           # focus on correct detections             
           pred_match_indices = pred_match[(pred_match>-1)].astype(int) # For each correctly predicted box it has the index of the matched gt box. 
           gt_match_scores = class_gt_score[pred_match_indices] # gt scores of each correctly predicted box
           gt_match_instance_ids = class_gt_instance_id[pred_match_indices] # gt instance_ids of each correctly predicted box
           
           # only consider ground truth scores > -1
           positive_gt_score_indices = np.where(class_gt_score>-1) # indices of positive gt scores
           gt_positive_scores = class_gt_score[positive_gt_score_indices]
           
            
           # if relevant predictions are existing, calculate metrics, otherwise skip
           if len(pred_gt_instance_id)>0:
           
               #perform evaluation for original and for corrected scores: 
               for scores in [r["scores"], np.array(corrected_scores)]:
               
                   class_pred_scores = scores[class_pred_indices]
                   pred_gt_scores = np.where(gt_match>-1 ,class_pred_scores[gt_match.astype(int)], np.nan)  

                   #get uncertainties of predictions 
                   score_epistemic = pred_score_epistemic[positive_gt_score_indices] 
                   score_aleatoric = pred_score_aleatoric[positive_gt_score_indices] 
           
                   # predictions with ground truth scores > -1 - this also filters classes that are different thann 1 as these have -1 scores
                   pred_positive_gt_scores = pred_gt_scores[positive_gt_score_indices]
                    
                   # calculate error for those gt that are positive and have a prediction
                   error = np.full_like(pred_gt_scores, np.nan)
                   error[positive_gt_score_indices] = np.absolute(np.subtract(gt_positive_scores, pred_positive_gt_scores))
                   error = np.where(pred_gt_scores < 0, np.nan, error)
                      
                   if scores is r["scores"]:
                       pred_gt_scores_original = pred_gt_scores 
                       squared_errors_original = np.square(error)
                       correct_class_original = np.where(squared_errors_original < 0.25, 1, 0)
                       correct_class_original = np.where(np.isnan(squared_errors_original), np.nan, correct_class_original)
                    
                   else:
                       pred_gt_scores_corrected = pred_gt_scores 
                       squared_errors_corrected = np.square(error)
                       correct_class_corrected = np.where(squared_errors_corrected < 0.25, 1, 0)
                       correct_class_corrected = np.where(np.isnan(squared_errors_corrected), np.nan, correct_class_corrected)
            
               for gt_instance_index, gt_score_instance in enumerate(class_gt_score):
                   image_result[class_id].append({
                            "gt_instance_id": class_gt_instance_id[gt_instance_index],
                            "instance_id": pred_gt_instance_id[gt_instance_index],
                            "instance_count": len(class_gt_score),
                            "gt_score": gt_score_instance,
                            "pred_score": pred_gt_scores_original[gt_instance_index],
                            "pred_score_corrected": pred_gt_scores_corrected[gt_instance_index],
                            "score_epistemic": pred_score_epistemic[gt_instance_index],
                            "score_aleatoric": pred_score_aleatoric[gt_instance_index],
                            "squared_error_original": squared_errors_original[gt_instance_index],
                            "squared_error_corrected": squared_errors_corrected[gt_instance_index],  
                            "correct_class_original": correct_class_original[gt_instance_index],
                            "correct_class_corrected": correct_class_corrected[gt_instance_index]
                   })

       return image_result
              
       
def evaluate_dict(dict_results):
        """
        Compute accuracy and mean squared error (MSE) for a given dictionary of tracklet or identity results.

        Args:
            dict_results (dict): A dictionary of tracklet or identity results. Keys are tracklet IDs and values are lists of dictionaries,
                where each dictionary contains results for a single image.

        Returns:
            tuple: A tuple of dictionaries containing the original and corrected tracklet/identity-level accuracy and MSE per class
            as well as the final output of a tracklet/identity.
        """
        
        # Initialize dictionaries that contain the metrics for each tracklet/identity
        dict_accuracy_original = {}
        MSE_original = {}
        dict_accuracy_corrected = {}
        MSE_corrected = {}
        final_predictions = {}
        
        # Loop over all dicts
        for dict_key in dict_results.keys():
            if dict_key not in dict_accuracy_original:
                dict_accuracy_original[dict_key] = {}
                MSE_original[dict_key] = {}
            if dict_key not in dict_accuracy_corrected:
                dict_accuracy_corrected[dict_key] = {}
                MSE_corrected[dict_key] = {}
                final_predictions[dict_key] = {}
            
            # Initialize dictionaries to keep track of correct and total counts 
            squared_errors_original = []
            correct_counts_original = 0
            total_counts_original = 0
            squared_errors_corrected = []
            correct_counts_corrected = 0
            total_counts_corrected = 0
            final_predictions[dict_key] = np.nan
            
            dict = dict_results[dict_key]
            # Loop over all images in the dict
            for image in dict:
                # Loop over all instances in the image
                for instance_result in image:
                    # Add to the correct count if entry is not NaN
                    if not np.isnan(instance_result['correct_class_original']):
                        # add squared errors
                        squared_errors_original.append(instance_result['squared_error_original'])
                        # add counts 
                        correct_counts_original += instance_result['correct_class_original']
                        total_counts_original += 1
                        
                    if not np.isnan(instance_result['correct_class_corrected']):
                        # add squared errors
                        squared_errors_corrected.append(instance_result['squared_error_corrected'])
                        # add counts 
                        correct_counts_corrected += instance_result['correct_class_corrected']
                        total_counts_corrected += 1
                        final_predictions[dict_key] = [instance_result['pred_score_corrected'], instance_result['correct_class_corrected']]
                            
            # Compute MSE and accuracy 
                if total_counts_original > 0:
                    MSE_original[dict_key] = np.nanmean(squared_errors_original)
                    dict_accuracy_original[dict_key] = correct_counts_original / total_counts_original
                else:
                    dict_accuracy_original[dict_key] = np.nan
                    MSE_original[dict_key] = np.nan
                
                if total_counts_corrected > 0:
                    dict_accuracy_corrected[dict_key] = correct_counts_corrected/ total_counts_corrected
                    MSE_corrected[dict_key] = np.nanmean(squared_errors_corrected)
                else:
                    dict_accuracy_corrected[dict_key] = np.nan
                    MSE_corrected[dict_key] = np.nan
                    
     
        metrics_original = [dict_accuracy_original, MSE_original]
        metrics_corrected = [dict_accuracy_corrected, MSE_corrected]
        
        # return the orignal and corrected instance_level metrics per dict together with the final corrected prediction
        return metrics_original, metrics_corrected, final_predictions
        

def create_json(filename, data):
    # convert keys to strings
    data = [{str(key): value for key, value in d.items()} for d in data]
    
    # create json file
    date = datetime.datetime.now()
    with open(filename, "a") as f:
        json.dump(data, f, default=str)
    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='MaskRCNN Video Object Detection/Instance Segmentation')
    parser.add_argument('-t', '--type', type=str, required=True, choices=["chicken", "person"], help='chicken or person assessment')
    parser.add_argument('-u', '--uncertainty', type=str, required=True, choices=["aleatoric", "epistemic"], help='type of uncertainty used to weight predictions')
    parser.add_argument('-f', '--input_folder_path', type=str, required=True, help='Path to input folder')
    parser.add_argument('-gt', '--gt_annotations', type=str, required=False, help='Path to ground truth annotations')
    parser.add_argument('-v', '--video_path', type=str, default='', help='Path to export the images as a video sequence')
    parser.add_argument('-ip', '--images_path', type=str, default='images', help= 'Path to save the output images')
    args = parser.parse_args()
    uncertainty_type = args.uncertainty
    class_names = ["background", "hen", "naked_spot"]
    # get absolute path of input directory
    relative_path = args.input_folder_path
    if os.path.exists(relative_path):
        input_folder_path = os.path.abspath(relative_path)
    else:
        print("Cannot find " + relative_path)
        exit(1)
   
    # Directory of trained model
    MODEL_DIR = 'C:/Users/user/Documents/Thesis/Mask_RCNN'
    
    if args.type == "chicken":
        inference_config = ChickenConfig()
    elif args.type == "person":
        inference_config = MarsConfig()

    # Create model object in inference mode.
    model = modellib.MaskRCNN(mode="inference", config=inference_config, model_dir=MODEL_DIR)
    
    # initialize encoder
    model_filename = 'mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename, batch_size=1)
    
    # calculate cosine distance metric
    max_cosine_distance = 0.5
    nn_budget = 100
    metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
    # initialize tracker
    tracker = Tracker(metric, max_iou_distance=0.7, max_age=10, n_init=3)
    
    #initialize image array for video creation.
    path_name_array = []
    img_array = []
    image_count = 0

    if args.type == "chicken":
        #load dataset 
        dataset = ChickenDataset()
        #dataset.load_data('/media/christian/SamsungSSD/ZED/datasets/tracking_dataset/hen_tracking_test/releases/coco_tracking_test/ids+scores.json', input_folder_path)
        dataset.load_data('/media/christian/SamsungSSD/ZED/datasets/tracking_dataset/hen_tracking_val/releases/coco_tracking_val/ids+scores.json', input_folder_path)
        dataset.prepare()
        
        # Load weights
        #model_path = os.path.join(ROOT_DIR, '/media/christian/SamsungSSD/tensorflow_logs/occlusion_dataset_aleatoric_loss_3/mask_rcnn_feather__damage_0109.h5')
        model_path = os.path.join(ROOT_DIR, '/media/christian/SamsungSSD/tensorflow_logs/larger_dataset_aleatoric_calibrated_factor05onaleatoric_2/mask_rcnn_feather__damage_0095.h5') #71, 60, 67, 97
        print("Loading weights from ", model_path)
        model.load_weights(model_path, by_name=True)
    
    elif args.type == "person":
        #load dataset 
        dataset = MarsDataset()
        dataset.load_data('/media/christian/SamsungSSD/MARS_dataset/mars_attributes.csv', input_folder_path)
        dataset.prepare()
        
        # Load weights
        model_path = os.path.join(ROOT_DIR, "/media/christian/SamsungSSD/tensorflow_logs/age_estimation_calibrated20230406T1552/mask_rcnn_age_estimation_calibrated_0500.h5")
        print("Loading weights from ", model_path)
        model.load_weights(model_path, by_name=True)
    
    


    # Inititialize counts for correct instances (original and corrected) as well as for correct tracklets and identites. The latter ones are only relevant for predictions on tracklet/identity level
    total_instances_original = [0]*len(dataset.class_info) # instances for each class
    correct_instances_original = [0]*len(dataset.class_info)
    
    # Predictions can be corrected either based on identity or tracklet information 
    total_instances_corrected_tracklet = [0]*len(dataset.class_info)
    correct_instances_corrected_tracklet = [0]*len(dataset.class_info)
    total_instances_corrected_identity = [0]*len(dataset.class_info)
    correct_instances_corrected_identity = [0]*len(dataset.class_info)
    
    total_tracklets = [0]*len(dataset.class_info)
    correct_tracklets = [0]*len(dataset.class_info)
    
    total_identities =[0]*len(dataset.class_info)
    correct_identities = [0]*len(dataset.class_info)
    
    overall_final_identity_predictions = [{} for _ in range(len(dataset.class_info))] 

    #create dict that only contains path and image id for faster lookup
    path_dict = {element['path']: index for index, element in enumerate(dataset.image_info)}
    image_number = len(path_dict)
    subdirs =  glob.glob(os.path.join(input_folder_path, "*", ""))
    print("len subdirs", len(subdirs))
    if len(subdirs) == 0:
       subdirs = [os.path.join(input_folder_path, '')]
    folder_index = 0
    
    for dir in subdirs:   #Each subdirectory corresponds to one or more tracklets of which each can include one or more identities 
        folder_name = dir.split("/")[-2]
        folder_index +=1 
        if os.path.exists(f"evaluation_epistemic_20_instances_cluster_examples/{folder_name}"):
            print(f"The folder {folder_name} exists in the directory.")
            continue
   
        i = 0
        #inititalize dict for instance-level results of gt tracklets per class
        tracklet_results = [{} for _ in range(len(dataset.class_info))] 
        #inititalize dict of all gt individual identities per class
        identity_results = [{} for _ in range(len(dataset.class_info))] 
        
        #inititalize dict for preds/uncertainties of detected identities/individuals per class
        individuals_dict = [{} for _ in range(len(dataset.class_info))] 
        #inititalize dict for preds/uncertainties of detected identities/individuals per tracklet per class
        tracklet_individuals_dict = [{} for _ in range(len(dataset.class_info))] 
         
        for filename in sorted(glob.glob(os.path.join(dir, 'left*.png'))):
        #for filename in sorted(glob.glob(os.path.join(dir, '*.jpg'))): 
            try:
                image_id = path_dict[filename]
                print("filename", filename, " image_id", image_id) 
            except:
                continue
            i+=1
            
            class_id = 1  # loop if multiple classes

            if eval:
                img, image_meta, gt_class_id, gt_bbox, gt_mask, gt_score, gt_quality, gt_instance_id = modellib.load_image_gt(dataset, inference_config, image_id, use_depth=False)
                print("gt scores:", gt_score)
            else:
                img = load_image(filename, inference_config)
            
            
            # make prediction
            yhat = model.detect([img], verbose=1)
            # extract results for first sample
            r_original = yhat[0]
            display_instances(img, gt_bbox, gt_mask, gt_class_id, class_names, scores=gt_score, corrected_scores=gt_score, uncertainties=gt_score)
            #display_instances(img, r_original['rois'],r_original['masks'], r_original['class_ids'], class_names, scores=r_original['scores'], corrected_scores=r_original['scores'], uncertainties=r_original['score_variances'])
            if args.type == "person":
                tracklet_ids = [int(filename[-12:-8])]
            elif args.type == "chicken":
                tracklet_ids = [str(1) + '_' + str(id) for id in gt_instance_id] # Chicken sequences consists of 1 tracklet per individual and multiple tracklets per sequence
            print("Tracklet ID: ", tracklet_ids)
            
            
            # run assessment on tracklet level (resets information about identities after each tracklet)
            tracklet_id_found = False
            # Check if any tracklet_id is in tracklet_results[class_id]
            for tracklet_id in tracklet_ids:
                if tracklet_id in tracklet_results[class_id]:
                    tracklet_id_found = True
                    break

            # Update tracklet_individuals_dict based on the flag variable
            if not tracklet_id_found:
                tracklet_individuals_dict[class_id] = {}
    
            for tracklet_id in tracklet_ids:
                if tracklet_id not in tracklet_results[class_id]:
                    # initialize the tracklet_results_dict if new tracklet:
                    tracklet_results[class_id][tracklet_id] = []
                    # evaluate previous tracklet if existing 
                    if 'tracklet_final_prediction' in locals():
                        last_tracklet = list(tracklet_final_prediction.items())[-1]
                        last_prediction = last_tracklet[1]
                        if isinstance(last_prediction, list):
                            total_tracklets[class_id] += 1
                            correct_tracklets[class_id] += last_prediction[1] 
                        
            r, corrected_scores, ids, tracklet_individuals_dict[class_id] = run_assessment(tracker, encoder, img, r_original, tracklet_individuals_dict[class_id], uncertainty=uncertainty_type, gt_ids=gt_instance_id, gt_bbox=gt_bbox, gt_class_id=gt_class_id, gt_mask=gt_mask)
            pred_img = display_instances(img, r['rois'],r['masks'], r['class_ids'], class_names, scores=r['scores'], corrected_scores=corrected_scores, uncertainties=r['score_variances'], ids=ids)
            image_result = evaluate_image(filename, r, ids, corrected_scores, gt_class_id, gt_instance_id, gt_bbox, gt_mask, gt_score, gt_quality) 
            
            print("image result (based on tracklet): ", image_result)
            for class_index, class_result in enumerate(image_result):
                for instance in class_result:
                    # if chicken dataset, there are multiple tracklets in an image, so that tracklet id is defined after prediction 
                    if args.type == "chicken":
                        tracklet_id = str(1) + "_" + str(instance['gt_instance_id'])
                        gt_instance_id_new = str(instance['gt_instance_id']) + "_f" + str(folder_index)  # this is needed as the chicken id labels are only unique per folder, not overall. 
                        instance['gt_instance_id'] = gt_instance_id_new
                    elif args.type == "person":
                        tracklet_id = int(filename[-12:-8])
                    tracklet_results[class_index][tracklet_id].append([instance])
                    # add counts
                    if not np.isnan(instance['correct_class_original']):
                        total_instances_original[class_id] += 1
                        correct_instances_original[class_id] += instance['correct_class_original']
                    if not np.isnan(instance['correct_class_corrected']):
                        total_instances_corrected_tracklet[class_id] += 1
                        correct_instances_corrected_tracklet[class_id] += instance['correct_class_corrected']
            
            
            # run assessment on identity level
            r, corrected_scores, ids, individuals_dict[class_id] = run_assessment(tracker, encoder, img, r_original, individuals_dict[class_id], uncertainty=uncertainty_type, gt_ids=gt_instance_id, gt_bbox=gt_bbox, gt_class_id=gt_class_id, gt_mask=gt_mask)
            image_result = evaluate_image(filename, r, ids, corrected_scores, gt_class_id, gt_instance_id, gt_bbox, gt_mask, gt_score, gt_quality) 
            print("image result (based on identity): ", image_result)
            
            for class_index, class_result in enumerate(image_result):
                for instance in class_result:
                    gt_instance_id = instance['gt_instance_id']
                    if args.type == "chicken":
                        gt_instance_id = str(gt_instance_id) + "_f" + str(folder_index)  # this is needed as the chicken id labels are only unique per folder, not overall. 
                        instance['gt_instance_id'] = gt_instance_id
                    if gt_instance_id not in identity_results[class_index]:
                        identity_results[class_index][gt_instance_id] = []
                    identity_results[class_index][gt_instance_id].append([instance])
                    # add counts
                    if not np.isnan(instance['correct_class_corrected']):
                        total_instances_corrected_identity[class_id] += 1
                        correct_instances_corrected_identity[class_id] += instance['correct_class_corrected']
                       
            image_count += 1          
            # Compute original and corrected (instance-level) metrics per tracklet and per identity as well as final predictions per tracklet/identity
            # On tracklet level, information is resettet after each tracklet. On identity-level, identity information is kept over multiple tracklets
            print("----------Tracklets:----------")
            tracklet_metrics_original, tracklet_metrics_corrected, tracklet_final_prediction = evaluate_dict(tracklet_results[class_id])
            #print("tracklet_results", tracklet_results)
            print("Tracklet_accuracy_original", tracklet_metrics_original[0])
            print("Tracklet_accuracy_corrected", tracklet_metrics_corrected[0])
            print("Tracklet_MSE_original", tracklet_metrics_original[1])
            print("Tracklet_MSE_corrected", tracklet_metrics_corrected[1])
            print("Tracklet_final_predictions", tracklet_final_prediction)
            
            print("----------Identitys:----------")
            identity_metrics_original, identity_metrics_corrected, identity_final_prediction = evaluate_dict(identity_results[class_id])
            # update the final predictions per identity:
            overall_final_identity_predictions[class_id].update(identity_final_prediction)
            #print("identity_results", identity_results)
            print("Identity_accuracy_original", identity_metrics_original[0])
            print("Identity_accuracy_corrected", identity_metrics_corrected[0])
            print("Identity_MSE_original", identity_metrics_original[1])
            print("Identity_MSE_corrected", identity_metrics_corrected[1])
            print("Identity_final_predictions", identity_final_prediction)
            print("All_identity_final_predictions", overall_final_identity_predictions)

            print("----------Overall:----------")
            print("correct_instances_original", correct_instances_original[class_id])
            instance_level_accuracy_original = 0 if total_instances_original[class_id] == 0 else correct_instances_original[class_id]/total_instances_original[class_id]
            print("Instance_Level_Accuracy_Original: ", instance_level_accuracy_original)
            instance_level_accuracy_corrected_tracklet = 0 if total_instances_corrected_tracklet[class_id] == 0 else correct_instances_corrected_tracklet[class_id]/total_instances_corrected_tracklet[class_id]
            print("Instance_Level_Accuracy_Corrected_Tracklet: ", instance_level_accuracy_corrected_tracklet)
            instance_level_accuracy_corrected_identity = 0 if total_instances_corrected_identity[class_id] == 0 else correct_instances_corrected_identity[class_id]/total_instances_corrected_identity[class_id]
            print("Instance_Level_Accuracy_Corrected_Identity: ", instance_level_accuracy_corrected_identity)
            
            
            # Compute overall accuracy on tracklet level
            tracklet_level_accuracy = 0 if total_tracklets[class_id] == 0 else correct_tracklets[class_id]/total_tracklets[class_id]
            print("Tracklet_Level_Accuracy: ", tracklet_level_accuracy)
            
            # Compute overall accuracy on identity level
            total_identities[class_id] = sum(1 for val in overall_final_identity_predictions[class_id].values() if isinstance(val, (list, tuple)) and not np.isnan(val[0]))
            correct_identities[class_id] = sum(x[1] for x in overall_final_identity_predictions[class_id].values() if isinstance(x, list) and not np.isnan(x[0]))
            identity_level_accuracy = 0 if total_identities[class_id] == 0 else correct_identities[class_id]/total_identities[class_id]
            print("Identity_Level_Accuracy: ", identity_level_accuracy)
            
            print(f"\nProgress: {image_count/image_number*100:.2f}%")
            
  
        # Create a new folder for each evaluated directory if it doesn't already exist
        folder_name = dir.split("/")[-2]
        if not os.path.exists(f"evaluation_epistemic_20_instances_cluster_examples/{folder_name}"):
            print(f"create folder evaluation/{folder_name}")
            os.makedirs(f"evaluation_epistemic_20_instances_cluster_examples/{folder_name}")
            
        
        create_json(f"evaluation_epistemic_20_instances_cluster_examples/{folder_name}/tracklet_results.json", tracklet_results)
        create_json(f"evaluation_epistemic_20_instances_cluster_examples/{folder_name}/identity_results.json", identity_results)

        # create a dictionary containing the metrics for this folder
        data = {
            "Tracklet_accuracy_original": str(tracklet_metrics_original[0]),
            "Tracklet_accuracy_corrected": str(tracklet_metrics_corrected[0]),
            "Tracklet_MSE_original": str(tracklet_metrics_original[1]),
            "Tracklet_MSE_corrected": str(tracklet_metrics_corrected[1]),
            "Tracklet_final_predictions": str(tracklet_final_prediction),
            "Identity_accuracy_original": str(identity_metrics_original[0]),
            "Identity_accuracy_corrected": str(identity_metrics_corrected[0]),
            "Identity_MSE_original": str(identity_metrics_original[1]),
            "Identity_MSE_corrected": str(identity_metrics_corrected[1]),
            "Identity_final_predictions": str(identity_final_prediction)
        }

        # write the dictionary to a JSON file
        with open(f"evaluation_epistemic_20_instances_cluster_examples/{folder_name}/metrics.json", "w") as f:
            json.dump(data, f)
    
    final_data = {
        "instance_level_accuracy_original": str(instance_level_accuracy_original),
        "instance_level_accuracy_corrected_tracklet": str(instance_level_accuracy_corrected_tracklet),
        "instance_level_accuracy_corrected_identity": str(instance_level_accuracy_corrected_identity),
        "tracklet_level_accuracy": str(tracklet_level_accuracy),
        "identity_level_accuracy": str(identity_level_accuracy)
    }
    with open(f"evaluation_epistemic_20_instances_cluster_examples/metrics.json", "w") as f:
            json.dump(final_data, f)
        
        # # Save image to the new folder
        # skimage.io.imsave(os.path.join(folder_name,str(i) + ".png"), pred_img)
        # height, width, layers = img.shape
        # size = (width,height)
        # img_array.append(pred_img)
        # path_name_array.append(filename) 
            
        # #img_array = [x for _, x in sorted(zip(path_name_array, img_array))]
     
        # #Total - write settings and those result that are printed to file 

        # # if path is defined, output video 
        # if args.video_path != '':
            # out = cv2.VideoWriter(args.video_path,cv2.VideoWriter_fourcc(*'DIVX'), 15, size)
            # for i in range(len(img_array)):
                # out.write(cv2.cvtColor(img_array[i], cv2.COLOR_RGB2BGR))
            # print("release video, containing no of images is:", len(img_array))
            # print("file: ", out)
            # out.release()
  
