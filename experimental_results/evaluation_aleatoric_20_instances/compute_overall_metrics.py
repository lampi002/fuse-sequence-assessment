import os    
import json  
import re    
import math 
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np 
from collections import defaultdict
from sklearn.metrics import accuracy_score, f1_score, mean_squared_error

       
def calculate_accuracies(max_instances=20, max_tracklets=None):
    
    instances_per_tracklet = defaultdict(list) # dict that stores number of tracklets and instances per tracklet, as this information cannot be obtained from the identity_results file but is needed to limit the number of tracklets per identity
    instances_per_tracklet_verify = defaultdict(list)
    
    
    tracklet_correct_verify = 0
    tracklet_total_original = 0
    tracklet_total_verify = 0
    tracklet_rejected = 0
    
    instance_predictions_original_gt = []
    instance_predictions_original = []
    instance_predictions_tracklet_correct_veryfied_gt = []
    instance_predictions_tracklet_correct_veryfied = []
    instance_predictions_identity_corrected_gt = []
    instance_predictions_identity_corrected = []
    tracklet_predictions_corrected = []
    tracklet_predictions_gt = []
    tracklet_averages = []
    tracklet_averages_gt = []
    identity_predictions_corrected = []
    identity_predictions_gt = []
    identity_averages = []
    identity_averages_gt = []
    
    
    identity_correct_verify = 0
    identity_total_verify = 0
    identity_total_original = 0
    identity_rejected = 0
    previous_gt_instance_id = None 
    

    
    
    # Get the path of the current script
    metrics_dir = os.path.dirname(os.path.abspath(__file__))
    
    for folder in os.listdir(metrics_dir):
        #print("folder", folder)
        if folder == "0000" or folder == "00-1":
           continue
        if os.path.isdir(os.path.join(metrics_dir, folder)):
            with open(os.path.join(metrics_dir, folder, "tracklet_results.json")) as f:
                tracklet_results = json.load(f)
                for class_data in tracklet_results:
                    if class_data:
                        original_predictions_sum_identity = 0
                        count_original_predictions_identity = 0
                        no_tracklets = len(class_data.items())
                        for tracklet_count, (tracklet_id, tracklet_data) in enumerate(class_data.items()):
                            if not tracklet_data or not tracklet_data[0] or not tracklet_data[0][0]:
                                #if tracklet is empty, find out instance_id and add 0 to instances_per_tracklet (only relevant for MARS dataset, chicken has only one tracklet which is not empty)
                                for tracklet_data in class_data.values():
                                   if tracklet_data:
                                      gt_instance_id = tracklet_data[0][0].get("gt_instance_id")
                                      break
                                instances_per_tracklet[gt_instance_id].append(0)
                                continue
                            # exclude those objects that are not labeled
                            gt_instance_id = tracklet_data[0][0].get("gt_instance_id")
                            # reset identity count if id changes. 
                            
                            #print("count_original_predictions_identity", count_original_predictions_identity)
                            if gt_instance_id != previous_gt_instance_id:
                                original_predictions_sum_identity = 0
                                count_original_predictions_identity = 0
                            if tracklet_data[0][0].get("gt_score") == "-1.0":
                                   instances_per_tracklet[gt_instance_id].append(0)
                                   continue
                            original_predictions_sum_tracklet = 0
                            tracklet_total_original += 1
                            count_original_predictions_tracklet = 0
                            no_instances = len(tracklet_data)
                            instances_per_tracklet[gt_instance_id].append(no_instances)
                            # Flag to track not-detected instances (relevant if the amount of instances is manually limited)
                            has_detected = False
                            for instance, instance_data in enumerate(tracklet_data):
                                gt_score = float(instance_data[0].get("gt_score"))
                                pred_score = float(instance_data[0].get("pred_score"))
                                if not math.isnan(pred_score):
                                    #print(instance)
                                    count_original_predictions_tracklet +=1 
                                    count_original_predictions_identity +=1
                                    original_predictions_sum_tracklet += pred_score
                                    original_predictions_sum_identity += pred_score
                                    gt_score_class = int(gt_score)
                                    corrected_score = float(instance_data[0].get("pred_score_corrected"))
                                    correct_class_original = instance_data[0].get("correct_class_original")
                                    if not math.isnan(correct_class_original):
                                        instance_predictions_original_gt.append(gt_score_class)
                                        instance_predictions_original.append(float(pred_score))
                                    correct_class_tracklet_correct_verifyion = instance_data[0].get("correct_class_corrected")
                                    if not math.isnan(correct_class_tracklet_correct_verifyion):
                                        instance_predictions_tracklet_correct_veryfied_gt.append(gt_score_class)
                                        instance_predictions_tracklet_correct_veryfied.append(corrected_score)
                                    has_detected = True
                                if instance == max_instances-1:
                                    break; 

                            #evaluate final prediction of tracklet 
                            if not math.isnan(correct_class_tracklet_correct_verifyion) and has_detected:
                                    tracklet_predictions_corrected.append(corrected_score)
                                    tracklet_predictions_gt.append(gt_score_class)
                            else:
                                tracklet_rejected+=1
                                
                            # compute average original predictions among tracklet 
                            if (count_original_predictions_tracklet > 0):
                                average_prediction_tracklet = original_predictions_sum_tracklet / count_original_predictions_tracklet
                                tracklet_averages.append(average_prediction_tracklet)
                                tracklet_averages_gt.append(int(gt_score))
                            
                            if max_tracklets and tracklet_count >= max_tracklets-1:
                                    break; 
                                    
                                    
                        # compute average original predictions among identity
                        if gt_instance_id != previous_gt_instance_id:
                            if (count_original_predictions_identity > 0):
                                average_prediction_identity = original_predictions_sum_identity / count_original_predictions_identity
                                identity_averages.append(average_prediction_identity)
                                identity_averages_gt.append(int(gt_score))
                                #print("new gt_instance_id", gt_instance_id, " previous_gt_instance_id", previous_gt_instance_id, " average_prediction_identity", average_prediction_identity, "count_original_predictions_identity", count_original_predictions_identity ) 
                            original_predictions_sum_identity = 0
                            count_original_predictions_identity = 0
                            previous_gt_instance_id = gt_instance_id

                               
                            
                                    
                                        
            with open(os.path.join(metrics_dir, folder, "identity_results.json")) as f:
                identity_results = json.load(f)
                for class_data in identity_results:
                    unlabeled_or_undetected_count = 0 
                    if isinstance(class_data, dict) and len(class_data) > 0:      
                        current_index_in_tracklet = 0
                        for identity_id, identity_data in class_data.items():
                            current_tracklet_index = 0
                            correct_class_identity_correction = np.nan
                            verify_no_instances = len(identity_data)
                            for instance, instance_data in enumerate(identity_data):
                                # if the tracklet is empty, go to next one:
                                last_tracklet_empty = False
                                #print(instances_per_tracklet[identity_id], identity_id, current_tracklet_index, current_index_in_tracklet)
                                while instances_per_tracklet[identity_id][current_tracklet_index] == 0:
                                    current_tracklet_index +=1
                                    if current_tracklet_index >= len(instances_per_tracklet[identity_id]):
                                        last_tracklet_empty = True
                                        current_tracklet_index = 0
                                        break;
                                if last_tracklet_empty:
                                    break 
                                if not max_tracklets or current_tracklet_index < max_tracklets:
                                    if current_index_in_tracklet < max_instances:
                                        if not math.isnan(instance_data[0].get("correct_class_corrected")):
                                            gt_score = float(instance_data[0].get("gt_score"))
                                            pred_score_corrected = instance_data[0].get("pred_score_corrected")
                                            correct_class_identity_correction = instance_data[0].get("correct_class_corrected")
                                        if not math.isnan(correct_class_identity_correction) and not math.isnan(float(instance_data[0].get("pred_score"))):
                                            instance_predictions_identity_corrected_gt.append(int(gt_score))
                                            instance_predictions_identity_corrected.append(pred_score_corrected)
                                    current_index_in_tracklet += 1   
                                    if current_index_in_tracklet >= instances_per_tracklet[identity_id][current_tracklet_index]:
                                        current_tracklet_index += 1
                                        instances_per_tracklet_verify[identity_id] = current_tracklet_index
                                        current_index_in_tracklet = 0
                                        # if last tracklet of id:
                                        if current_tracklet_index == len(instances_per_tracklet[identity_id]):
                                            current_tracklet_index  = 0 
                                            break;
                                         
                            #evaluate final prediction of identity
                            #print(instance_data[0].get("pred_score_corrected"))
                            #print("correct_class_identity_correction", correct_class_identity_correction) 
                            if not math.isnan(correct_class_identity_correction):
                                identity_predictions_corrected.append(pred_score_corrected)
                                identity_predictions_gt.append(int(gt_score))
                            else: 
                                identity_rejected +=1
                                print("ID:", identity_id)
                                print("instances_per_tracklet[identity_id]", instances_per_tracklet[identity_id])
                                                         
                        
            with open(os.path.join(metrics_dir, folder, "metrics.json")) as f: 
                metrics = json.load(f)
                tracklet_predictions_string = metrics["Tracklet_final_predictions"]
                identity_predictions_string = metrics["Identity_final_predictions"]
                tracklet_predictions_string = re.sub(r'([{,])(\s*)([0-9]+)(\s*):', r'\1"\3":', tracklet_predictions_string)
                identity_predictions_string = re.sub(r'([{,])(\s*)([0-9]+)(\s*):', r'\1"\3":', identity_predictions_string)
                tracklet_predictions_string = tracklet_predictions_string.replace("nan", "null")
                identity_predictions_string = identity_predictions_string.replace("nan", "null")
                tracklet_predictions_string = tracklet_predictions_string.replace("'", "\"")
                identity_predictions_string = identity_predictions_string.replace("'", "\"")
                tracklet_predictions = json.loads(tracklet_predictions_string)
                identity_predictions = json.loads(identity_predictions_string)
                tracklet_predictions = json.loads(tracklet_predictions_string)
                identity_predictions = json.loads(identity_predictions_string)
                
                # Calculate tracklet level accuracy
                for tracklet_id, tracklet_data in tracklet_predictions.items():
                    if tracklet_data is not None:
                        tracklet_total_verify += 1
                        if int(tracklet_data[1]) == 1:
                            tracklet_correct_verify += 1
                
                # Calculate identity level accuracy
                if len(identity_predictions) > 0:
                    for identity_id in identity_predictions:
                        identity_data = identity_predictions[identity_id]
                        #print("identity_data", identity_data)
                        if identity_data is not None:
                            identity_total_verify += 1
                            if int(identity_data[1]) == 1:
                                identity_correct_verify += 1
                        else: print("None: ", identity_id)
                        
    

    tracklet_accuracy = tracklet_correct_verify / tracklet_total_verify if tracklet_total_verify != 0 else 0
    identity_accuracy = identity_correct_verify / identity_total_verify if identity_total_verify != 0 else 0
    
    accuracy_original = accuracy_score(instance_predictions_original_gt, np.round(instance_predictions_original))
    mse_original = mean_squared_error(instance_predictions_original_gt, instance_predictions_original)
    accuracy_tracklet_correct_veryfied = accuracy_score(instance_predictions_tracklet_correct_veryfied_gt, np.round(instance_predictions_tracklet_correct_veryfied))
    mse_tracklet_correct_veryfied = mean_squared_error(instance_predictions_tracklet_correct_veryfied_gt, instance_predictions_tracklet_correct_veryfied)
    accuracy_identity_corrected = accuracy_score(instance_predictions_identity_corrected_gt, np.round(instance_predictions_identity_corrected))
    mse_identity_corrected = mean_squared_error(instance_predictions_identity_corrected_gt, instance_predictions_identity_corrected)
    tracklet_accuracy = accuracy_score(tracklet_predictions_gt, np.round(tracklet_predictions_corrected))
    mse_tracklet = mean_squared_error(tracklet_predictions_gt, tracklet_predictions_corrected)
    tracklet_average_accuracy = accuracy_score(tracklet_averages_gt, np.round(tracklet_averages))
    mse_tracklet_average = mean_squared_error(tracklet_averages_gt, tracklet_averages)
    identity_accuracy = accuracy_score(identity_predictions_gt, np.round(identity_predictions_corrected))
    mse_identity = mean_squared_error(identity_predictions_gt, identity_predictions_corrected )
    identity_average_accuracy = accuracy_score(identity_averages_gt, np.round(identity_averages))
    mse_identity_average = mean_squared_error(identity_averages_gt, identity_averages)

    print(tracklet_rejected, " tracklets rejected")
    print(identity_rejected, " identities rejected")

    print("Accuracy Original new: ", accuracy_original, len(instance_predictions_original))
    print("MSE Score Original new: ", mse_original, len(instance_predictions_original))
    print("Accuracy Tracklet-corrected new : ", accuracy_tracklet_correct_veryfied, len(instance_predictions_tracklet_correct_veryfied))
    print("MSE Score Tracklet-corrected new : ", mse_tracklet_correct_veryfied, len(instance_predictions_tracklet_correct_veryfied))
    print("Accuracy Identity-corrected new : ", accuracy_identity_corrected, len(instance_predictions_identity_corrected))
    print("MSE Score Identity-corrected new : ", mse_identity_corrected, len(instance_predictions_identity_corrected))
    #print("Tracklet Accuracy Verify: ", tracklet_accuracy, tracklet_correct_verify, tracklet_total_verify)
    print("Tracklet Accuracy new: ", tracklet_accuracy, len(tracklet_predictions_corrected))
    print("MSE Score Tracklet new: ", mse_tracklet, len(tracklet_predictions_corrected))
    print("Tracklet Average Accuracy new: ", tracklet_average_accuracy, len(tracklet_averages))
    print("MSE Score Tracklet Average new: ", mse_tracklet_average, len(tracklet_averages))
    print(identity_total_verify, "Identities Evaluated ", identity_correct_verify, " correct")
    #print("Identity Accuracy: ", identity_accuracy,  identity_correct_verify, identity_total_verify)
    print("Identity Accuracy new: ", identity_accuracy, len(identity_predictions_corrected))
    print("MSE Score Identity new: ", mse_identity, len(identity_predictions_corrected))
    print("Identity Average Accuracy new: ", identity_average_accuracy, len(identity_averages))
    print("MSE Score Identity Average new: ", mse_identity_average, len(identity_averages))
    
    from collections import Counter
    # Count occurrences of numbers
    counts = Counter(instance_predictions_original_gt)

    # Print the counts
    for number, count in counts.items():
        print(f"Number {number} occurs {count} times")
    
    return accuracy_original, accuracy_tracklet_correct_veryfied, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy, mse_original, mse_tracklet_correct_veryfied, mse_identity_corrected, mse_tracklet, mse_tracklet_average, mse_identity, mse_identity_average



def generate_heatmap(result):
    sns.heatmap(result, annot=True, cmap='coolwarm')
    plt.title('Result 1')
    plt.savefig("plot.png")
    

def create_line_chart(data, x_label, y_labels, filename):
    # Extract x and y data from data pairs
    x_data = [pair[0][0] for pair in data]
    y_data = [[result for result in pair[1]] for pair in data]

    # Create dataframe from x and y data
    df = pd.DataFrame(y_data, columns=y_labels)
    df[x_label] = x_data
    df = df.set_index(x_label)

    # Create line plot using Seaborn
    plt.figure(figsize=(10, 6))
    sns.set(style="ticks", palette="colorblind")
    sns.lineplot(data=df, dashes=False, markers=True)

    # Set plot labels and title
    plt.xlabel(x_label)
    plt.ylabel("Accuracy")
    plt.title("Accuracies vs. " + x_label)
    
     # Move legend to the right of the plot
    plt.legend(bbox_to_anchor=(1.05, 0.5), loc='center left', borderaxespad=0.)
    # Adjust layout to prevent legend from being cut off
    plt.tight_layout()
    
    # Show the plot
    plt.savefig(filename, format = 'svg', dpi=300)
    
    
calculate_accuracies(max_tracklets=None)


max_tracklets_array = [1, 2, 5, 10, 30, 50, 80, 100]   
# #max_tracklets_array = [None] # For chicken dataset  
max_instances_array = [3, 4 , 5, 7, 10, 12, 15, 20] #, 50, 100]  

# create line charts:
y_labels = ['instance_level_original', 'instance_level_tracklet_correct_veryfied', 'instance_level_identity_corrected', 'tracklet', 'identity']
y_labels_with_average = ['instance_level_original', 'instance_level_tracklet_correct_veryfied', 'instance_level_identity_corrected', 'tracklet', 'identity', "tracklet_average", "identity_average"]

data_accuracy_tracklets = []
data_accuracy_tracklets_with_average = []
data_mse_tracklets = []
data_mse_tracklets_with_average = []

for tracklet_no in max_tracklets_array:
     print("---------------")
     print("Evaluating Performance with max_tracklets = ", tracklet_no)
     accuracy_original, accuracy_tracklet_correct_veryfied, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy, mse_original, mse_tracklet_correct_veryfied, mse_identity_corrected, mse_tracklet, mse_tracklet_average, mse_identity, mse_identity_average = calculate_accuracies(max_tracklets=tracklet_no, max_instances=100000)
     data_accuracy_tracklets.append([[tracklet_no]*5, [accuracy_original, accuracy_tracklet_correct_veryfied, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy]])
     data_accuracy_tracklets_with_average.append([[tracklet_no]*7, [accuracy_original, accuracy_tracklet_correct_veryfied, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy]])
     data_mse_tracklets.append([[tracklet_no]*5, [mse_original, mse_tracklet_correct_veryfied, mse_identity_corrected, mse_tracklet, mse_identity]])
     data_mse_tracklets_with_average.append([[tracklet_no]*7, [mse_original, mse_tracklet_correct_veryfied, mse_identity_corrected, mse_tracklet, mse_identity, mse_tracklet_average, mse_identity_average]])

create_line_chart(data_accuracy_tracklets, 'Number of Tracklets', y_labels , "line_chart_tracklets.svg")
create_line_chart(data_accuracy_tracklets_with_average, 'Number of Tracklets', y_labels_with_average , "line_chart_tracklets_w_average.svg")
create_line_chart(data_mse_tracklets, 'Number of Tracklets', y_labels , "line_chart_tracklets_MSE.svg")
create_line_chart(data_mse_tracklets_with_average, 'Number of Tracklets', y_labels_with_average , "line_chart_tracklets_w_average_MSE.svg")
      
data_accuracy_instances = [] 
data_accuracy_instances_with_average = []
data_mse_instances = [] 
data_mse_instances_with_average = []
for instance_no in max_instances_array:
    print("---------------")
    print("Evaluating Performance with max_instances = ", instance_no)
    accuracy_original, accuracy_tracklet_correct_veryfied, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy, mse_original, mse_tracklet_correct_veryfied, mse_identity_corrected, mse_tracklet, mse_tracklet_average, mse_identity, mse_identity_average = calculate_accuracies(max_tracklets=None, max_instances=instance_no)
    data_accuracy_instances.append([[instance_no]*5, [accuracy_original, accuracy_tracklet_correct_veryfied, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy]]) 
    data_accuracy_instances_with_average.append([[instance_no]*7, [accuracy_original, accuracy_tracklet_correct_veryfied, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy]]) 
    data_mse_instances.append([[instance_no]*5, [mse_original, mse_tracklet_correct_veryfied, mse_identity_corrected, mse_tracklet, mse_identity]])
    data_mse_instances_with_average.append([[instance_no]*7, [mse_original, mse_tracklet_correct_veryfied, mse_identity_corrected, mse_tracklet, mse_identity, mse_tracklet_average, mse_identity_average]])
create_line_chart(data_accuracy_instances, 'Number of Instances per Tracklet', y_labels, "line_chart_instances.svg")
create_line_chart(data_accuracy_instances_with_average, 'Number of Instances per Tracklet', y_labels_with_average, "line_chart_instances_w_average.svg")
create_line_chart(data_mse_instances, 'Number of Instances per Tracklet', y_labels, "line_chart_instances_MSE.svg")
create_line_chart(data_mse_instances_with_average, 'Number of Instances per Tracklet', y_labels_with_average, "line_chart_instances_w_average_MSE.svg")
 

#results = []
#data_pairs_tracklets_x_instances = []
##max_tracklets_array = [1, 2, 3, 4, 5]   
##max_instances_array = [3, 4, 5, 6, 7, 8, 9, 10]  
#for tracklet_no in max_tracklets_array:
#    for instance_no in max_instances_array:
#        print("---------------")
#        print("MAX Tracklets: ", tracklet_no)
#        print("MAX Instances: ", instance_no)
#        accuracy_original, accuracy_tracklet_correct_veryfied, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy = calculate_accuracies(max_tracklets=tracklet_no, max_instances=instance_no)
#        data_pairs_tracklets_x_instances.append([[instance_no*tracklet_no]*7, [accuracy_original, accuracy_tracklet_correct_veryfied, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy]]) 
#        # results.append([tracklet_no, instance_no, accuracy_original, accuracy_tracklet_correct_veryfied, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy])

#create_line_chart(data_pairs_tracklets_x_instances, 'Total number of Instances', y_labels_with_average, "line_chart_tracklets_x_instances.svg")
#calculate_accuracies(max_tracklets=None)

# # create heatmap 
# # df = pd.DataFrame(results, columns=['Max Tracklets', 'Max Instances', 'Result 1', 'Result 2', 'Result 3'])
# # # Create a pivot table for each result
# # df_result1 = df.pivot('Max Tracklets', 'Max Instances', 'Result 1')
# # df_result2 = df.pivot('Max Tracklets', 'Max Instances', 'Result 2')
# # df_result3 = df.pivot('Max Tracklets', 'Max Instances', 'Result 3')

# # # generate_heatmap(df_result1)
