import os    
import json  
import re    
import math 
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np 
from collections import defaultdict

       
def calculate_accuracies(max_instances=10, max_tracklets=None):
    
    instances_per_tracklet = defaultdict(list) # dict that stores number of tracklets and instances per tracklet, as this information cannot be obtained from the identity_results file but is needed to limit the number of tracklets per identity
    instances_per_tracklet_verify = defaultdict(list)
    
    instance_correct_original = 0
    instance_total_original = 0
    instance_correct_tracklet_correction = 0
    instance_total_tracklet_correction = 0
    instance_correct_identity_correction = 0
    instance_total_identity_correction = 0
    
    tracklet_correct = 0
    tracklet_correct_2 = 0
    tracklet_total_original = 0
    tracklet_total = 0
    tracklet_total_2 = 0
    tracklet_total_3 = 0
    tracklet_total_4 = 0
    tracklet_rejected = 0
    tracklet_average_correct = 0 
    
    identity_correct = 0
    identity_correct_2 = 0
    identity_total = 0
    identity_total_original = 0
    identity_total_2 = 0 
    identity_rejected = 0
    identity_average_correct = 0
    
    
    
    # Get the path of the current script
    metrics_dir = os.path.dirname(os.path.abspath(__file__))
    
    for folder in os.listdir(metrics_dir):
        if os.path.isdir(os.path.join(metrics_dir, folder)):
            #print("folder: ", folder)
            
            with open(os.path.join(metrics_dir, folder, "tracklet_results.json")) as f:
                tracklet_results = json.load(f)
                for class_data in tracklet_results:
                    if class_data:
                        # get gt id: 
                        for tracklet_data in class_data.values():
                            if tracklet_data:
                                gt_instance_id = tracklet_data[0][0].get("gt_instance_id")
                                break
                        original_predictions_sum_identity = 0
                        count_original_predictions_identity = 0  
                        no_tracklets = len(class_data.items())
                        for tracklet_count, (tracklet_id, tracklet_data) in enumerate(class_data.items()):
                            original_predictions_sum_tracklet = 0
                            tracklet_total_original += 1
                            count_original_predictions_tracklet = 0
                            no_instances = len(tracklet_data)
                            instances_per_tracklet[gt_instance_id].append(no_instances)
                            for instance, instance_data in enumerate(tracklet_data):
                                count_original_predictions_tracklet +=1 
                                count_original_predictions_identity +=1
                                gt_score = float(instance_data[0].get("gt_score"))
                                original_predictions_sum_tracklet += float(instance_data[0].get("pred_score"))
                                original_predictions_sum_identity += float(instance_data[0].get("pred_score"))
                                correct_class_original = instance_data[0].get("correct_class_original")
                                if not math.isnan(correct_class_original):
                                    instance_total_original += 1
                                    if correct_class_original == 1:
                                        instance_correct_original += 1
                                correct_class_tracklet_correction = instance_data[0].get("correct_class_corrected")
                                if not math.isnan(correct_class_tracklet_correction):                            
                                    instance_total_tracklet_correction += 1
                                    if correct_class_tracklet_correction == 1:
                                        instance_correct_tracklet_correction += 1
                                if instance == max_instances-1:
                                    break; 

                            #evaluate final prediction of tracklet 
                            if not math.isnan(correct_class_tracklet_correction):
                                    tracklet_total_2 += 1
                                    if correct_class_tracklet_correction == 1:
                                        tracklet_correct_2 += 1 
                            else: 
                                #print("tracklet ", tracklet_id, " in folder ", folder , " rejected")
                                tracklet_rejected+=1
                            # compute average original predictions among tracklet 
                            if (count_original_predictions_tracklet > 0):
                                average_prediction_tracklet = original_predictions_sum_tracklet / count_original_predictions_tracklet
                                if round(average_prediction_tracklet) == gt_score:
                                    tracklet_average_correct += 1

                            if max_tracklets and tracklet_count == max_tracklets-1:
                                    break; 
                                    
                        # compute average original predictions among identity 
                        if (count_original_predictions_identity > 0):
                            average_prediction_identity = original_predictions_sum_identity / count_original_predictions_identity
                            if round(average_prediction_identity) == gt_score:
                                identity_average_correct += 1
                                    
                                        
            with open(os.path.join(metrics_dir, folder, "identity_results.json")) as f:
                identity_results = json.load(f)
                for class_data in identity_results:
                    if isinstance(class_data, dict) and len(class_data) > 0:      
                        current_tracklet_index = 0
                        current_index_in_tracklet = 0
                        for identity_id, identity_data in class_data.items():
                            verify_no_instances = len(identity_data)
                            tracklet_total_4 += len(instances_per_tracklet[identity_id])
                            for instance, instance_data in enumerate(identity_data):
                                # if the tracklet is empty, go to next one:
                                last_tracklet_empty = False
                                while instances_per_tracklet[identity_id][current_tracklet_index] == 0:
                                    current_tracklet_index +=1
                                    tracklet_total_3 += 1
                                    if current_tracklet_index == len(instances_per_tracklet[identity_id]):
                                        last_tracklet_empty = True
                                        break;
                                if last_tracklet_empty:
                                    break 
                                if not max_tracklets or current_tracklet_index < max_tracklets:
                                    if current_index_in_tracklet < max_instances:
                                        correct_class_identity_correction = instance_data[0].get("correct_class_corrected")
                                        if not math.isnan(correct_class_identity_correction):
                                            instance_total_identity_correction += 1
                                            if correct_class_identity_correction == 1:
                                                instance_correct_identity_correction += 1   
                                    current_index_in_tracklet += 1  
                                    if current_index_in_tracklet >= instances_per_tracklet[identity_id][current_tracklet_index]:
                                        current_tracklet_index += 1
                                        instances_per_tracklet_verify[identity_id] = current_tracklet_index
                                        tracklet_total_3 += 1
                                        current_index_in_tracklet = 0
                                        if current_tracklet_index == len(instances_per_tracklet[identity_id]):
                                            break;

                        
                            #evaluate final prediction of identity 
                            if not math.isnan(correct_class_identity_correction):
                                identity_total_2 += 1
                                if correct_class_identity_correction == 1:
                                    identity_correct_2 += 1 
                            else: identity_rejected +=1
                                                         
                        
            with open(os.path.join(metrics_dir, folder, "metrics.json")) as f:
            
                metrics = json.load(f)
                tracklet_predictions_string = metrics["Tracklet_final_predictions"]
                identity_predictions_string = metrics["Identity_final_predictions"]
                tracklet_predictions_string = re.sub(r'([{,])(\s*)([0-9]+)(\s*):', r'\1"\3":', tracklet_predictions_string)
                identity_predictions_string = re.sub(r'([{,])(\s*)([0-9]+)(\s*):', r'\1"\3":', identity_predictions_string)
                tracklet_predictions_string = tracklet_predictions_string.replace("nan", "null")
                identity_predictions_string = identity_predictions_string.replace("nan", "null")
                tracklet_predictions = json.loads(tracklet_predictions_string)
                identity_predictions = json.loads(identity_predictions_string)
                tracklet_predictions = json.loads(tracklet_predictions_string)
                identity_predictions = json.loads(identity_predictions_string)
                
                # Calculate tracklet level accuracy
                for tracklet_id, tracklet_data in tracklet_predictions.items():
                    if tracklet_data is not None:
                        tracklet_total += 1
                        if int(tracklet_data[1]) == 1:
                            tracklet_correct += 1
                
                # Calculate identity level accuracy
                if len(identity_predictions) > 0:
                    identity_id = list(identity_predictions.keys())[-1]
                    identity_data = identity_predictions[identity_id]
                    if identity_data is not None:
                        identity_total += 1
                        if int(identity_data[1]) == 1:
                            identity_correct += 1
                    else:
                        print("Identity None")
                        print("identity_id", identity_id)
    
    accuracy_original = instance_correct_original / instance_total_original
    accuracy_tracklet_corrected = instance_correct_tracklet_correction / instance_total_tracklet_correction if instance_total_tracklet_correction != 0 else 0
    accuracy_identity_corrected = instance_correct_identity_correction / instance_total_identity_correction if instance_total_identity_correction != 0 else 0
    tracklet_accuracy = tracklet_correct / tracklet_total if tracklet_total != 0 else 0
    tracklet_accuracy_2 = tracklet_correct_2 / tracklet_total_2 if tracklet_total_2 != 0 else 0
    tracklet_average_accuracy = tracklet_average_correct / tracklet_total_original if tracklet_total_original != 0 else 0
    identity_accuracy = identity_correct / identity_total if identity_total != 0 else 0
    identity_accuracy_2 = identity_correct_2 / identity_total_2 if identity_total_2 != 0 else 0
    identity_average_accuracy = identity_average_correct / identity_total if identity_total != 0 else 0
    
    print(tracklet_total, " Orignal Tacklets")
    print(identity_total, " Original Identities")
    
    print(tracklet_total_2, "Tracklet predictions", tracklet_rejected, " tracklets rejected")
    print(tracklet_total_3, " (Validation) Tracklets from identity results")
    print(tracklet_total_4, " (Validation) Tracklets from instances_per_tracklet ")
    print(identity_total_2, " identities evaluated")
    
    print("Accuracy Original: ", accuracy_original, instance_correct_original, instance_total_original)
    print("Accuracy Tracklet-corrected: ", accuracy_tracklet_corrected)
    print("Accuracy Identity-corrected: ", accuracy_identity_corrected)
    print("Tracklet Accuracy: ", tracklet_accuracy)
    print("Tracklet Accuracy 2: ", tracklet_accuracy_2, tracklet_correct_2 , tracklet_total_2)
    print("Tracklet Average Accuracy: ", tracklet_average_accuracy, tracklet_average_correct, tracklet_total_original)
    print(identity_total , "Identities Evaluated ", identity_correct, " correct")
    print("Identity Accuracy: ", identity_accuracy)
    print(identity_total_2 , "Identities Evaluated ", identity_correct_2, " correct", identity_rejected, " rejected")
    print("Identity Accuracy_2: ", identity_accuracy_2, identity_correct_2 , identity_total_2 )
    print("Identity Average Accuracy: ", identity_average_accuracy, identity_average_correct , identity_total )
    return accuracy_original, accuracy_tracklet_corrected, accuracy_identity_corrected, tracklet_accuracy_2, identity_accuracy_2, tracklet_average_accuracy, identity_average_accuracy




def generate_heatmap(result):
    sns.heatmap(result, annot=True, cmap='coolwarm')
    plt.title('Result 1')
    plt.savefig("plot.png")
    

def create_line_chart(data, x_label, y_labels, filename):
    # Extract x and y data from data pairs
    x_data = [pair[0][0] for pair in data]
    y_data = [[result for result in pair[1]] for pair in data]

    # Create dataframe from x and y data
    df = pd.DataFrame(y_data, columns=y_labels)
    df[x_label] = x_data
    df = df.set_index(x_label)

    # Create line plot using Seaborn
    plt.figure(figsize=(10, 6))
    sns.set(style="ticks", palette="colorblind")
    sns.lineplot(data=df, dashes=False, markers=True)

    # Set plot labels and title
    plt.xlabel(x_label)
    plt.ylabel("Accuracy")
    plt.title("Accuracies vs. " + x_label)
    
     # Move legend to the right of the plot
    plt.legend(bbox_to_anchor=(1.05, 0.5), loc='center left', borderaxespad=0.)
    # Adjust layout to prevent legend from being cut off
    plt.tight_layout()
    
    # Show the plot
    plt.savefig(filename, format = 'svg', dpi=300)
    
    
calculate_accuracies(max_tracklets=None)

max_tracklets_array = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]   
# max_instances_array = [3, 4, 5, 6, 7, 8, 9, 10]  

# create line charts:
y_labels = ['instance_level_original', 'instance_level_tracklet_corrected', 'instance_level_identity_corrected', 'tracklet', 'identity']
y_labels_with_average = ['instance_level_original', 'instance_level_tracklet_corrected', 'instance_level_identity_corrected', 'tracklet', 'identity', "tracklet_average", "identity_average"]

data_pairs_tracklets = []
data_pairs_tracklets_with_average = []
for tracklet_no in max_tracklets_array:
    accuracy_original, accuracy_tracklet_corrected, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy = calculate_accuracies(max_tracklets=tracklet_no, max_instances=100000)
    data_pairs_tracklets.append([[tracklet_no]*5, [accuracy_original, accuracy_tracklet_corrected, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy]])
    data_pairs_tracklets_with_average.append([[tracklet_no]*7, [accuracy_original, accuracy_tracklet_corrected, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy]])
create_line_chart(data_pairs_tracklets, 'Number of Tracklets', y_labels , "line_chart_tracklets.svg")
create_line_chart(data_pairs_tracklets_with_average, 'Number of Tracklets)', y_labels_with_average , "line_chart_tracklets_w_average.svg")
      
# data_pairs_instances = [] 
# data_pairs_instances_with_average = []
# for instance_no in max_instances_array:
    # accuracy_original, accuracy_tracklet_corrected, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy = calculate_accuracies(max_tracklets=2, max_instances=instance_no)
    # data_pairs_instances.append([[instance_no]*5, [accuracy_original, accuracy_tracklet_corrected, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy]]) 
    # data_pairs_instances_with_average.append([[instance_no]*7, [accuracy_original, accuracy_tracklet_corrected, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy]]) 
# create_line_chart(data_pairs_instances, 'Number of Instances (Max_tracklets=2)', y_labels, "line_chart_instances.svg")
# create_line_chart(data_pairs_instances_with_average, 'Number of Instances (Max_tracklets=2)', y_labels_with_average, "line_chart_instances_w_average.svg")
 

# results = []
# data_pairs_tracklets_x_instances = []
# #max_tracklets_array = [1, 2, 3, 4, 5]   
# #max_instances_array = [3, 4, 5, 6, 7, 8, 9, 10]  
# for tracklet_no in max_tracklets_array:
    # for instance_no in max_instances_array:
        # print("---------------")
        # print("MAX Tracklets: ", tracklet_no)
        # print("MAX Instances: ", instance_no)
        # accuracy_original, accuracy_tracklet_corrected, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy = calculate_accuracies(max_tracklets=tracklet_no, max_instances=instance_no)
        # data_pairs_tracklets_x_instances.append([[instance_no*tracklet_no]*7, [accuracy_original, accuracy_tracklet_corrected, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy]]) 
        # # results.append([tracklet_no, instance_no, accuracy_original, accuracy_tracklet_corrected, accuracy_identity_corrected, tracklet_accuracy, identity_accuracy, tracklet_average_accuracy, identity_average_accuracy])

# create_line_chart(data_pairs_tracklets_x_instances, 'Total number of Instances', y_labels_with_average, "line_chart_tracklets_x_instances.svg")
# calculate_accuracies(max_tracklets=None)

# # create heatmap 
# # df = pd.DataFrame(results, columns=['Max Tracklets', 'Max Instances', 'Result 1', 'Result 2', 'Result 3'])
# # # Create a pivot table for each result
# # df_result1 = df.pivot('Max Tracklets', 'Max Instances', 'Result 1')
# # df_result2 = df.pivot('Max Tracklets', 'Max Instances', 'Result 2')
# # df_result3 = df.pivot('Max Tracklets', 'Max Instances', 'Result 3')

# # # generate_heatmap(df_result1)
